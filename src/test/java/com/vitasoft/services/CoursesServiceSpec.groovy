package com.vitasoft.services

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.vitasoft.models.CourseModel
import com.vitasoft.repositories.CourseRepository
import org.junit.Test
import spock.lang.Specification

class CoursesServiceSpec extends Specification {

    @Collaborator
    private CourseRepository courseRepository = Mock()

    @Subject
    private CoursesService coursesService

    @Test
    def "should throw IllegalArgumentException when course not found for id"() {
        given:
        courseRepository.findById(34) >> Optional.empty()

        when:
        coursesService.getCourse(34)

        then:
        thrown(IllegalArgumentException.class)
    }

    @Test
    def "should return course when course found for id"() {
        given:
        courseRepository.findById(34) >> Optional.of(new CourseModel())

        when:
        coursesService.getCourse(34)

        then:
        1 * courseRepository.findById(34)
    }
}
