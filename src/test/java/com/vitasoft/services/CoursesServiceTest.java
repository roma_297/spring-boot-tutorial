package com.vitasoft.services;

import com.vitasoft.models.CourseModel;
import com.vitasoft.repositories.CourseRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CoursesServiceTest {
	
	@Mock
	private CourseRepository courseRepository;
	
	@InjectMocks
	private CoursesService coursesService;
	
	@BeforeClass
	public static void setUpClass() {
		System.out.println("Before whole test suite");
	}
	
	@Before
	public void setUp() {
		System.out.println("Before each test");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionWhenCourseNotFoundForId() {
//		Given
		Mockito.when(courseRepository.findById(34)).thenReturn(Optional.empty());

//		When
		coursesService.getCourse(34);
	}
	
	@Test
	public void shouldReturnCourseModelWhenCourseFoundForId() {
//		Given
		CourseModel courseMock = Mockito.mock(CourseModel.class);
		Mockito.when(courseRepository.findById(34)).thenReturn(Optional.of(courseMock));

//		When
		CourseModel course = coursesService.getCourse(34);

//		Then
		Assert.assertEquals(courseMock, course);
		Mockito.verify(courseRepository).findById(34);
	}
	
	@Test
	public void shouldReturnAllCoursesInCorrectOrder() {
//		Given
		CourseModel courseMock1 = Mockito.mock(CourseModel.class);
		Mockito.when(courseMock1.getOrderIndex()).thenReturn(23);
		CourseModel courseMock2 = Mockito.mock(CourseModel.class);
		Mockito.when(courseMock1.getOrderIndex()).thenReturn(22);
		Mockito.when(courseRepository.findAll()).thenReturn(Arrays.asList(courseMock1, courseMock2));

//		When
		List<CourseModel> allCourses = coursesService.getAllCourses();

//		Then
		Assert.assertEquals(2, allCourses.size());
		Assert.assertTrue(allCourses.get(0).getOrderIndex() < allCourses.get(1).getOrderIndex());
	}
	
	
}