package com.vitasoft.facades;

import static org.mockito.ArgumentMatchers.eq;

import com.vitasoft.dto.CourseDto;
import com.vitasoft.models.CourseModel;
import com.vitasoft.services.CoursesService;
import java.util.Collections;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class CoursesFacadeTest {
	
	@Captor
	private ArgumentCaptor<CourseModel> courseModelArgumentCaptor;
	
	private CoursesService coursesService;
	@Mock
	private ConversionService conversionService;
	
	@InjectMocks
	private CoursesFacade coursesFacade;
	
	@Before
	public void setUp() {
		coursesService = Mockito.mock(CoursesService.class);
		ReflectionTestUtils.setField(coursesFacade, "coursesService", coursesService);
	}
	
	@Test
	public void shouldReturnAllCourses() {
		Mockito.when(coursesService.getAllCourses()).thenReturn(Collections.singletonList(new CourseModel(23, "name", "description", 239D, 12)));
		
		coursesFacade.getAllCourses();
		
		Mockito.verify(conversionService).convert(courseModelArgumentCaptor.capture(), eq(CourseDto.class));
		CourseModel courseModel = courseModelArgumentCaptor.getValue();
		Assert.assertEquals(23, (int) courseModel.getId());
	}
	
	@After
	public void destroy() {
		System.out.println("After each test");
	}
}