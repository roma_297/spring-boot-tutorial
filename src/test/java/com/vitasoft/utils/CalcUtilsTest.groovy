package com.vitasoft.utils

import spock.lang.Specification

class CalcUtilsTest extends Specification {

    CalcUtils calcUtils = new CalcUtils()

    def "should sum to numbers"(int a, int b, int c) {
        expect:
        c == calcUtils.add(a, b)

        where:
        a   | b  | c
        1   | 2  | 4
        213 | 2  | 215
        1   | -4 | -3
    }

}
