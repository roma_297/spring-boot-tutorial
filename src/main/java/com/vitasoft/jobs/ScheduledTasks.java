package com.vitasoft.jobs;

import java.time.LocalTime;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
	
	@Scheduled(fixedRate = 5000)
	public void method1() {
		System.out.println("method1 was called");
	}
	
	@Scheduled(fixedDelay = 15000)
	public void method2() {
		System.out.println("method2 was called");
	}

	@Scheduled(cron = "0-45 * 21 * * *")
	public void method3() {
		System.out.println(LocalTime.now());
	}
}
