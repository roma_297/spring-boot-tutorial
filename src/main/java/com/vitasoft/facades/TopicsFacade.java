package com.vitasoft.facades;

import com.vitasoft.dto.TopicDto;
import com.vitasoft.services.CoursesService;
import com.vitasoft.services.TopicService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class TopicsFacade {
	
	private CoursesService coursesService;
	private TopicService topicService;
	private ConversionService conversionService;
	
	public List<TopicDto> getTopics() {
		return topicService.getTopics();
	}
	
	public TopicDto getTopic(int topicId) {
		return topicService.getTopic(topicId);
	}
	
	public List<TopicDto> getTopicsForCourse(int courseId) {
		return CollectionUtils.emptyIfNull(coursesService.getCourse(courseId).getTopics()).stream()
			.map(topic -> conversionService.convert(topic, TopicDto.class))
			.collect(Collectors.toList());
	}
}
