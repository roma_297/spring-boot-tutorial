package com.vitasoft.facades;

import com.vitasoft.dto.CourseDto;
import com.vitasoft.models.CourseModel;
import com.vitasoft.services.CoursesService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class CoursesFacade {
	
	private CoursesService coursesService;
	private ConversionService conversionService;
	
	public List<CourseDto> getAllCourses() {
		return CollectionUtils.emptyIfNull(coursesService.getAllCourses()).stream()
			.map(course -> conversionService.convert(course, CourseDto.class))
			.collect(Collectors.toList());
	}
	
	public CourseDto getCourse(int id) {
		CourseModel course = coursesService.getCourse(id);
		return conversionService.convert(course, CourseDto.class);
	}
	
	public void removeCourse(int id) {
		coursesService.removeCourse(id);
	}
}
