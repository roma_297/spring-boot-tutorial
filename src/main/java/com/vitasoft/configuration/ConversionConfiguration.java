package com.vitasoft.configuration;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ConversionConfiguration extends WebMvcConfigurerAdapter {
	
	@Autowired
	private List<Converter> converters;
	
	@Override
	public void addFormatters(FormatterRegistry formatterRegistry) {
		CollectionUtils.emptyIfNull(converters)
			.forEach(formatterRegistry::addConverter);
	}
	
}