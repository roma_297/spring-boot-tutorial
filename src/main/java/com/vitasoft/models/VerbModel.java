package com.vitasoft.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@DiscriminatorValue("verb")
@Getter
@Setter
public class VerbModel extends AbstractWordModel {
	
	private boolean isRegular;
}
