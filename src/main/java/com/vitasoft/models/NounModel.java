package com.vitasoft.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@DiscriminatorValue("noun")
@Getter
@Setter
public class NounModel extends AbstractWordModel {
	private String article;
}
