package com.vitasoft.models;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class CourseModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String description;
	private Double price;
	@Transient
	private Double usDollarsPrice;
	private Integer orderIndex;
	@OneToMany
	private List<TopicModel> topics;
	
	public CourseModel() {
	}
	
	public CourseModel(int id, String name, String description, double price, int orderIndex) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.orderIndex = orderIndex;
	}
}
