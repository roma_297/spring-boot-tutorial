package com.vitasoft.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class TopicModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
}
