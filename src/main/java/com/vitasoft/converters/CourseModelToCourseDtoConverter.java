package com.vitasoft.converters;

import com.vitasoft.dto.CourseDto;
import com.vitasoft.models.CourseModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CourseModelToCourseDtoConverter implements Converter<CourseModel, CourseDto> {
	
	@Nullable
	@Override
	public CourseDto convert(CourseModel courseModel) {
		CourseDto courseDto = new CourseDto();
		
		courseDto.setId(courseModel.getId());
		courseDto.setName(courseModel.getName());
		courseDto.setDescription(courseModel.getDescription());
		courseDto.setPrice(courseModel.getPrice());
		
		return courseDto;
	}
}
