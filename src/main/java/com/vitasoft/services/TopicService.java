package com.vitasoft.services;

import com.vitasoft.dto.TopicDto;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class TopicService {
	
	public List<TopicDto> getTopics() {
		return null;
	}
	
	public TopicDto getTopic(int topicId) {
		return null;
	}
}
