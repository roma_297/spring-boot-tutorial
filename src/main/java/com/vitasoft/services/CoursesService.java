package com.vitasoft.services;

import com.vitasoft.models.CourseModel;
import com.vitasoft.repositories.CourseRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class CoursesService {
	
	private CourseRepository courseRepository;
	
	public List<CourseModel> getAllCourses() {
		List<CourseModel> courseModels = new ArrayList<>();
		
		courseRepository.findAll().forEach(courseModels::add);
		
		return courseModels.stream()
			.sorted(Comparator.comparingInt(CourseModel::getOrderIndex))
			.collect(Collectors.toList());
	}
	
	public CourseModel getCourse(int id) {
		return courseRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(""));
	}
	
	public void removeCourse(int id) {
		courseRepository.deleteById(id);
	}
}
