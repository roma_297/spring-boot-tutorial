package com.vitasoft.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseDto {
	
	private int id;
	private String name;
	private String description;
	private double price;
}
