package com.vitasoft.forms;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseForm {
	
	@NotNull
	@Size(min = 2, max = 255)
	private String name;
	@NotNull
	@Size(min = 2, max = 255)
	private String description;
	@NotNull
	@DecimalMax(value = "10000")
	private double price;
	@Min(0)
	@Max(1000)
	private int order;
}
