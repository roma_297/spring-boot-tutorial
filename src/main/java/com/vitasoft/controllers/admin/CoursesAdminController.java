package com.vitasoft.controllers.admin;

import com.vitasoft.facades.CoursesFacade;
import com.vitasoft.forms.CourseForm;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Controller
@RequestMapping("/admin/courses")
public class CoursesAdminController {
	
	@Resource
	private CoursesFacade coursesFacade;
	
	@GetMapping
	public ModelAndView getCoursesPage() {
		return new ModelAndView("pages/admin/coursesPage")
			.addObject("courses", coursesFacade.getAllCourses())
			.addObject("courseForm", new CourseForm());
	}
	
	@PostMapping
	public ModelAndView addCourse(@Valid CourseForm courseForm, Errors errors) {
		
		if (errors.hasErrors()) {
			return new ModelAndView("pages/admin/coursesPage")
				.addObject("courses", coursesFacade.getAllCourses());
		}
		
		return new ModelAndView(UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/admin/courses");
	}
	
	
}
