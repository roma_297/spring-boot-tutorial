package com.vitasoft.controllers.user;

import com.vitasoft.dto.CourseDto;
import com.vitasoft.dto.TopicDto;
import java.util.Arrays;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainPageController {
	
	@GetMapping("/main")
	public String getMainPage(Model model) {
		model.addAttribute("intValue", 3);
		model.addAttribute("list", Arrays.asList(1, 3, 5));
		
		CourseDto courseDto1 = new CourseDto();
		courseDto1.setName("Java course");
		courseDto1.setDescription("");
		courseDto1.setPrice(10000);
		
		CourseDto courseDto2 = new CourseDto();
		courseDto2.setName("Python course");
		courseDto2.setDescription("");
		courseDto2.setPrice(13000);
		
		CourseDto courseDto3 = new CourseDto();
		courseDto3.setName("Kotlin course");
		courseDto3.setDescription("");
		courseDto3.setPrice(12400);
		
		model.addAttribute("courses", Arrays.asList(courseDto1, courseDto2, courseDto3));
		return "pages/mainPage";
	}
	
	@GetMapping("/")
	public ModelAndView getEmptyPage() {
		return new ModelAndView("pages/mainPage");
	}
	
}
