package com.vitasoft.controllers.user;

import com.vitasoft.dto.TopicDto;
import com.vitasoft.facades.TopicsFacade;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/courses/{courseId}/topics")
public class TopicsController {
	
	private final TopicsFacade topicsFacade;
	
	@GetMapping
	public List<TopicDto> getTopics(@PathVariable int courseId) {
		return topicsFacade.getTopicsForCourse(courseId);
	}
	
	@GetMapping("/{topicId}")
	public TopicDto getTopic(int topicId) {
		return topicsFacade.getTopic(topicId);
	}
	
	
}
