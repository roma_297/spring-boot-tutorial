package com.vitasoft.controllers.user;

import com.vitasoft.dto.CourseDto;
import com.vitasoft.facades.CoursesFacade;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/courses")
public class CoursesPageController {
	
	private CoursesFacade coursesFacade;
	
	@GetMapping
	public List<CourseDto> getAllCourses() {
		return coursesFacade.getAllCourses();
	}
	
	@GetMapping("/{id}")
	public CourseDto getCourse(@PathVariable int id) {
		return coursesFacade.getCourse(id);
	}
	
	@DeleteMapping("/{id}")
	public void removeCourse(@PathVariable int id) {
		coursesFacade.removeCourse(id);
	}
	
}
