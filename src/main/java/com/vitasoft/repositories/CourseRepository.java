package com.vitasoft.repositories;

import com.vitasoft.models.CourseModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<CourseModel, Integer> {
	
	CourseModel findCourseModelByName(String name);
	
	CourseModel findCourseModelByNameLike(String name);
	
	@Query(value = "SELECT course FROM CourseModel course WHERE course.name = :name")
	CourseModel findCourseModelCustom(@Param("name") String name);
}
